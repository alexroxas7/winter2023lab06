public class Jackpot {
	public static void main(String[] args){
		
		System.out.println("---WELCOME TO THE GAME OF JACKPOT!!!---");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		
		while(!gameOver){
			System.out.println(board);
			if(board.playATurn()){
				gameOver = true;
			} else {
				numOfTilesClosed++;
			} 
		}
		
		if (numOfTilesClosed >= 7){
			System.out.println("YOU HAVE REACHED THE JACKPOT AND WON!");
		} else {
			System.out.println("You have lost the game of Jackpot :(");	
		}
	}
}