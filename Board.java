public class Board {
	
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board() {
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString(){
		String tileNum = "";
		
		for(int i = 0; i < this.tiles.length; i++){
			if(tiles[i]){
				tileNum += "X ";
			} else {
				tileNum += i+1 + " ";
			}
		}
		return tileNum;
	}
	
	public boolean playATurn(){
		die1.roll();
		die2.roll();
		System.out.println("First die: " + die1.getFaceValue());
		System.out.println("Second die: " + die2.getFaceValue());
		
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		if (!this.tiles[sumOfDice - 1]) {
			this.tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		
		if (!tiles[die1.getFaceValue() - 1]) {
            this.tiles[die1.getFaceValue() - 1] = true;
            System.out.println("Closing tile with the same value as die one: " + die1.getFaceValue());
            return false;
        }

		if (!tiles[die2.getFaceValue() - 1]) {
            this.tiles[die2.getFaceValue() - 1] = true;
            System.out.println("Closing tile with the same value as die two: " + die2.getFaceValue());
            return false;
        }

		System.out.println("All the tiles for these values are already shut");
		return true;
	}
	
}